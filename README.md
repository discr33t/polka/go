# go

Install and manage Golang

## Dependencies

* [polka.asdf](https://gitlab.com/discr33t/polka/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`
    * Type: List
    * Usages: A list of Golang versions to install

* `global_version`
    * Type: String
    * Usages: The Golang version to make the global default

```
golang:
  versions:
    - 1.10
    - 1.9
  global_version: 1.10
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.go

## License

MIT
